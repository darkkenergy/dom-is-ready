var win = require("win-wrapper")();

/**
 * @name DOMIsReady
 * @version 1.0.1
 * @author Claudio Nuñez Jr.
 * @desc Simple front-end CommonJS module - a handy function to ensure the DOM
 *      is fully ready before executing provided functionality. Inspired by the
 *      following:
 *          Diego Perini (https://github.com/dperini/ContentLoaded/tree/v1.2)
 *          Nic Bell (https://github.com/nicbell/liteready)
 *          Noor Dawod (https://github.com/noordawod/dom-ready)
 * 
 * @param win {Window} :Bound param from module internals.
 * @param context {object} :[Optional] Scope of the dom-is-ready-handler (@param::handler)
 * @param handler {function} :Code to run once the DOM is fully ready.
 *      ->param {Window} :A reference to the Window object (for ease of testing.)
 */
function DOMIsReady(win, context, handler) {
    if (document.readyState === "complete") {

        // Call the dom-is-ready-handler immediately - the DOM is ready!

        handler.call(context || null);
        return;
    }

    this.listeners[this.listeners.length] = handler.bind((context || null), win);
};

DOMIsReady.listeners = [];
DOMIsReady.notify = _notify.bind(DOMIsReady);

// Fires all listeners.
function _notify() {
    for (var i = 0; i < DOMIsReady.listeners.length; i++) {
        this.listeners[i]();
    }
}

// Sets up the dom-is-ready events.
function _onDOMReady(win, notify) {

    // Mature browsers.
    if (win.document.addEventListener) {
        win.document.addEventListener("DOMContentLoaded", notify);
    }

    // Internet Explorer.
    else if (win.document.attachEvent) {

        // IE supports onreadystatechange event.
        win.document.attachEvent("onreadystatechange", function() {
            if (win.document.readyState === "complete") {
                notify();
            }
        });

        // Very reliable when not inside a frame.
        if (win.document.documentElement.doScroll && win === win.top) {
            _tryScroll(win, notify);
        }
    }
}

// Trick by Diego Perini
// http://javascript.nwbox.com/IEContentLoaded/
function _tryScroll(win, notify) {
    try {
        win.document.documentElement.doScroll("left");
    }
    catch (e) {
        setTimeout(_tryScroll, 50);
        return;
    }

    notify();
}

// Initialize onDOMReady event system.
_onDOMReady(win, DOMIsReady.notify);

module.exports = DOMIsReady.bind(DOMIsReady, win);