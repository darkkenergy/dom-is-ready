# DOMIsReady

> Simple front-end CommonJS module - a handy function to ensure the DOM is fully ready before executing provided functionality.

## Install

```bash
npm i dom-is-ready -S
```

## Inclusion

```js
var DOMIsReady = require('dom-is-ready');
```

## Usage

```js
/**
 * @name DOMIsReady
 * @version 1.0.1
 * @author Claudio Nuñez Jr.
 * @desc Simple front-end CommonJS module - a handy function to ensure the DOM
 *      is fully ready before executing provided functionality. Inspired by the
 *      following:
 *          Diego Perini (https://github.com/dperini/ContentLoaded/tree/v1.2)
 *          Nic Bell (https://github.com/nicbell/liteready)
 *          Noor Dawod (https://github.com/noordawod/dom-ready)
 * 
 * @param context {object} :[Optional] Scope of the dom-is-ready-handler (@param::handler)
 * @param handler {function} :Code to run once the DOM is fully ready.
 *      ->param {Window} :A reference to the Window object (for ease of testing.)
 */

// DOMIsReady(context, handler)

var DOMIsReady = require('dom-is-ready');

// Do something when the DOM is ready!
DOMIsReady({ dom: "ready!" }, function(win) {
    console.log("The DOM is ", this.dom); // outputs: "The DOM is ready!"
    console.log("Body Element: ", win.document.body); // outputs the body element.
});
```